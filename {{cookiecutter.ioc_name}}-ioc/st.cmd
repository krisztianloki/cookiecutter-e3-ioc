require essioc

epicsEnvSet("ENGINEER", "{{ cookiecutter.full_name }} <{{ cookiecutter.email }}>"

iocshLoad("$(essioc_DIR)/common-config.iocsh")

## For commands to be run after iocInit, use the function afterInit()

iocInit()

date
